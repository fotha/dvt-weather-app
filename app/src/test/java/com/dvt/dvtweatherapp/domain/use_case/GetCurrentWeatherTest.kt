package com.dvt.dvtweatherapp.domain.use_case

import com.dvt.dvtweatherapp.data.repository.FakeCurrentWeatherRepository
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test

class GetCurrentWeatherTest{
    private lateinit var getCurrentWeather: GetCurrentWeather
    private lateinit var fakeRepository: FakeCurrentWeatherRepository

    @Before
    fun setUp(){
        fakeRepository = FakeCurrentWeatherRepository()
        getCurrentWeather = GetCurrentWeather(fakeRepository)
    }

    @Test
    fun `given a location it should get current weather`(){
        //given
        val currentWeather = getCurrentWeather

        //when

        //then
        assertThat(currentWeather).isNotNull()
    }
}