package com.dvt.dvtweatherapp.data.repository

import com.dvt.dvtweatherapp.common.Resource
import com.dvt.dvtweatherapp.domain.model.CurrentWeather
import com.dvt.dvtweatherapp.domain.model.Main
import com.dvt.dvtweatherapp.domain.model.Weather
import com.dvt.dvtweatherapp.domain.repository.CurrentWeatherRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

// This is a fake repository that simulate the access of data from the datasources
class FakeCurrentWeatherRepository: CurrentWeatherRepository {
    private val currentWeather =  CurrentWeather(main = Main(10.0,10.1, 10.22), name = "Hello", weather = listOf(
        Weather(main = "Rain")
    ))
    private var returnNetworkError = false

    override fun getCurrentWeather(): Flow<Resource<CurrentWeather>> {
        return flow { emit(Resource.Success(data = currentWeather)) }
    }
}