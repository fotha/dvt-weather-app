package com.dvt.dvtweatherapp

import android.Manifest
import android.location.Location
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import com.dvt.dvtweatherapp.presentation.MainScreen
import com.dvt.dvtweatherapp.ui.theme.DVTWeatherAppTheme
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import dagger.hilt.android.AndroidEntryPoint
import pub.devrel.easypermissions.EasyPermissions

@AndroidEntryPoint
class MainActivity : ComponentActivity(),
    EasyPermissions.PermissionCallbacks,
    EasyPermissions.RationaleCallbacks {
    private val RC_LOCATION_PERM = 124
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    // has a new Location
    private lateinit var locationCallback: LocationCallback

    // This will store current location info
    private var currentLocation: Location? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)



        setContent {
            DVTWeatherAppTheme {
                MainScreen()
                if (hasLocationPermissions())
                {

                    // Have permission, do the thing!
                    Toast.makeText(this, "TODO: Location things", Toast.LENGTH_LONG).show()
                }
                else
                {
                    // Ask for one permission
                    EasyPermissions.requestPermissions(
                        this,
                        getString(R.string.rationale_location),
                        RC_LOCATION_PERM,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                }

            }
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {

    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {

    }

    override fun onRationaleAccepted(requestCode: Int) {

    }

    override fun onRationaleDenied(requestCode: Int) {

    }
    override fun onRequestPermissionsResult(requestCode:Int,
                                            permissions:Array<String>,
                                            grantResults:IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

}
private fun hasLocationPermissions():Boolean {
    return EasyPermissions.hasPermissions(WeatherApp.instance, Manifest.permission.ACCESS_FINE_LOCATION)
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    DVTWeatherAppTheme {
        MainScreen()
    }
}