package com.dvt.dvtweatherapp.domain.use_case

import com.dvt.dvtweatherapp.common.Resource
import com.dvt.dvtweatherapp.domain.model.CurrentWeather
import com.dvt.dvtweatherapp.domain.model.ForecastWeather
import com.dvt.dvtweatherapp.domain.repository.ForecastWeatherRepository
import kotlinx.coroutines.flow.Flow

class GetForecastWeather(
    private val repository: ForecastWeatherRepository
) {
    operator fun invoke(): Flow<Resource<ForecastWeather>> {
        return repository.getForecastWeather()
    }
}