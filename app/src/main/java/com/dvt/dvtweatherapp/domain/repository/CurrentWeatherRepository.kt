package com.dvt.dvtweatherapp.domain.repository

import com.dvt.dvtweatherapp.common.Resource
import com.dvt.dvtweatherapp.domain.model.CurrentWeather
import kotlinx.coroutines.flow.Flow


interface CurrentWeatherRepository {
    fun getCurrentWeather(): Flow<Resource<CurrentWeather>>
}