package com.dvt.dvtweatherapp.domain.model

data class CurrentLocation(
    val lon: Double,
    val lat: Double
)
