package com.dvt.dvtweatherapp.domain.model

data class Main(
    val temp: Double,
    val temp_max: Double,
    val temp_min: Double
)
