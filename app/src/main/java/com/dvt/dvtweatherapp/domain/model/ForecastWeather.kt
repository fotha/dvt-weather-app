package com.dvt.dvtweatherapp.domain.model

import com.dvt.dvtweatherapp.data.remote.dto.forecast.CityDto
import com.dvt.dvtweatherapp.data.remote.dto.forecast.IntervalDto

data class ForecastWeather(
    val list: List<Interval>
)
