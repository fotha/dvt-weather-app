package com.dvt.dvtweatherapp.domain.use_case

import android.Manifest
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import androidx.core.app.ActivityCompat
import com.dvt.dvtweatherapp.WeatherApp
import com.dvt.dvtweatherapp.domain.model.CurrentLocation
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import java.util.*


class GetCurrentLocation() {
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    // has a new Location
    private lateinit var locationCallback: LocationCallback

    // This will store current location info
    private var currentLocation: Location? = null
    var latitude: Double? = null
    var longitude: Double? = null


    fun getLatitude(lat: Double?): Double? {
        return lat
    }

    fun getLongitude(lon: Double?): Double? {
        return lon
    }

    operator fun invoke(): CurrentLocation {
        var latitude: Double? = null
        var longitude: Double? = null
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(WeatherApp.instance)
        if (ActivityCompat.checkSelfPermission(
                WeatherApp.instance,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                WeatherApp.instance,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

        }
        fusedLocationClient.lastLocation.addOnCompleteListener() {
            val location: Location? = it.result
            if (location != null) {
                val geocoder = Geocoder(WeatherApp.instance, Locale.getDefault())
                val list: List<Address> =
                    geocoder.getFromLocation(location.latitude, location.longitude, 1)
                latitude = location.latitude
                longitude = location.longitude
                println(list[0].countryName)

            }


        }
        return CurrentLocation(longitude!!,latitude!!)
    }
}
