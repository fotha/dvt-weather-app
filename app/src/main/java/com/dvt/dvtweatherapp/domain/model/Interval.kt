package com.dvt.dvtweatherapp.domain.model

import com.dvt.dvtweatherapp.data.remote.dto.MainDto
import com.dvt.dvtweatherapp.data.remote.dto.WeatherDto
import com.dvt.dvtweatherapp.data.remote.dto.forecast.RainDto
import com.dvt.dvtweatherapp.data.remote.dto.forecast.SysForecastDto

data class Interval(
    val dt_txt: String,
    val main: MainDto,
    val weather: List<Weather>,
)
