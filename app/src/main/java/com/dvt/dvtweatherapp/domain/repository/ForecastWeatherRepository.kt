package com.dvt.dvtweatherapp.domain.repository

import com.dvt.dvtweatherapp.common.Resource
import com.dvt.dvtweatherapp.domain.model.ForecastWeather
import kotlinx.coroutines.flow.Flow

interface ForecastWeatherRepository {
    fun getForecastWeather(): Flow<Resource<ForecastWeather>>

}