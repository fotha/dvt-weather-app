package com.dvt.dvtweatherapp.domain.model

data class Weather(
    val main: String
)
