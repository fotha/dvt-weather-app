package com.dvt.dvtweatherapp.domain.model

import com.dvt.dvtweatherapp.data.remote.dto.*

data class CurrentWeather(
    val main: Main,
    val name: String,
    val weather: List<Weather>
)
