package com.dvt.dvtweatherapp.domain.use_case

import com.dvt.dvtweatherapp.common.Resource
import com.dvt.dvtweatherapp.domain.model.CurrentWeather
import com.dvt.dvtweatherapp.domain.repository.CurrentWeatherRepository
import kotlinx.coroutines.flow.Flow

class GetCurrentWeather(
    private val repository: CurrentWeatherRepository
) {
    operator fun invoke(): Flow<Resource<CurrentWeather>>{
        return repository.getCurrentWeather()
    }
}