package com.dvt.dvtweatherapp.presentation.view_model

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dvt.dvtweatherapp.common.Resource
import com.dvt.dvtweatherapp.domain.model.ForecastWeather
import com.dvt.dvtweatherapp.domain.model.Interval
import com.dvt.dvtweatherapp.domain.use_case.GetForecastWeather
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ForecastWeatherViewModel @Inject constructor(
   private val getForecastWeather: GetForecastWeather
): ViewModel() {
   private val _state = mutableStateOf(ForecastWeatherState())
   val state: State<ForecastWeatherState> = _state

   private val _eventFlow = MutableSharedFlow<UIEvent>()
   val eventFlow = _eventFlow.asSharedFlow()

   var arrayList = mutableListOf<Interval>()

   private var loadJob: Job? = null

   sealed class UIEvent{
      data class ShowSnackBar(val message: String): UIEvent()
   }

   fun loadForecastWeather(){
      loadJob?.cancel()
      loadJob = viewModelScope.launch {
         getForecastWeather().onEach {result ->
            when(result){
               is Resource.Success ->{
                  _state.value = state.value.copy(
                     forecastWeatherItem  = result.data?: null,
                     isLoading = false

                  )
                  println(state.value.forecastWeatherItem)
                  result.data?.list
                  var i = 0
                  arrayList.clear()
                  while (i< result.data?.list?.size!!){
                     arrayList.add(result.data?.list[i])
                     i += 8
                  }
               }
               is Resource.Error ->{
                  _state.value = state.value.copy(
                     forecastWeatherItem= result.data?: null,
                     isLoading = false
                  )
                  _eventFlow.emit(
                     UIEvent.ShowSnackBar(
                        result.message ?: "Unknown error"
                     )
                  )

               }
               is Resource.Loading ->{
                  _state.value = state.value.copy(
                     forecastWeatherItem = result.data?: null,
                     isLoading = true
                  )

               }
            }

         }.launchIn(this)
      }

   }
}