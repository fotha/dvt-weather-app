package com.dvt.dvtweatherapp.presentation.view_model

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dvt.dvtweatherapp.common.Resource
import com.dvt.dvtweatherapp.domain.use_case.GetCurrentWeather
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CurrentWeatherViewModel @Inject constructor(
    private val getCurrentWeather: GetCurrentWeather
): ViewModel() {
    private val _state = mutableStateOf(CurrentWeatherState())
    val state: State<CurrentWeatherState> = _state

    private val _eventFlow = MutableSharedFlow<UIEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    private var loadJob: Job? = null

    sealed class UIEvent{
        data class ShowSnackBar(val message: String): UIEvent()
    }

    fun loadCurrentWeather(){
        loadJob?.cancel()
        loadJob = viewModelScope.launch {
            getCurrentWeather().onEach {result ->
                when(result){
                    is Resource.Success ->{
                        _state.value = state.value.copy(
                            currentWeatherItem = result.data?: null,
                            isLoading = false
                        )
                        println(state.value.currentWeatherItem)

                    }
                    is Resource.Error ->{
                        _state.value = state.value.copy(
                            currentWeatherItem = result.data?: null,
                            isLoading = false
                        )
                        _eventFlow.emit(
                            UIEvent.ShowSnackBar(
                                result.message ?: "Unknown error"
                            )
                        )

                    }
                    is Resource.Loading ->{
                        _state.value = state.value.copy(
                            currentWeatherItem = result.data?: null,
                            isLoading = true
                        )

                    }
                }

            }.launchIn(this)
        }

    }
}