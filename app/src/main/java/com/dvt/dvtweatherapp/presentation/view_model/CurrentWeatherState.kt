package com.dvt.dvtweatherapp.presentation.view_model

import com.dvt.dvtweatherapp.domain.model.CurrentWeather

data class CurrentWeatherState(
    val currentWeatherItem: CurrentWeather? = null,
    val isLoading: Boolean = false
)
