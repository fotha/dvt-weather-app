package com.dvt.dvtweatherapp.presentation.view_model
import com.dvt.dvtweatherapp.domain.model.ForecastWeather

data class ForecastWeatherState(
    val forecastWeatherItem: ForecastWeather? = null,
    val isLoading: Boolean = false
)