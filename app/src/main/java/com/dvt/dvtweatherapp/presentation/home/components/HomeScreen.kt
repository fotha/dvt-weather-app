package com.dvt.dvtweatherapp.presentation.home.components

import android.os.Build
import androidx.annotation.RequiresApi
import com.dvt.dvtweatherapp.R
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.dvt.dvtweatherapp.domain.model.ForecastWeather
import com.dvt.dvtweatherapp.domain.model.Interval
import com.dvt.dvtweatherapp.presentation.view_model.CurrentWeatherViewModel
import com.dvt.dvtweatherapp.presentation.view_model.ForecastWeatherViewModel
import kotlinx.coroutines.flow.collectLatest
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Period
import java.time.format.DateTimeFormatter
import java.util.*

var intervals = mutableListOf<Interval>()
var cout = 1

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun HomeScreen() {
    val viewModel: CurrentWeatherViewModel = hiltViewModel()
    val forecastViewModel: ForecastWeatherViewModel = hiltViewModel()
    val state = viewModel.state.value
    val arrayInterval = forecastViewModel.arrayList
    val forecastState = forecastViewModel.state.value
    val scaffoldState = rememberScaffoldState()

    var myModifier: Modifier = Modifier
    if (state.currentWeatherItem?.weather?.get(0)?.main == "Clouds") {
        myModifier = Modifier.background(Color(0xFF54717A))
    }
    if (state.currentWeatherItem?.weather?.get(0)?.main == "Clear") {
        myModifier = Modifier.background(Color(0xFF47AB2F))
    }
    if (state.currentWeatherItem?.weather?.get(0)?.main == "Rain") {
        myModifier = Modifier.background(Color(0xFF57575D))
    }
    val sdf = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
    val date = LocalDateTime.now().plusDays(1)


    println(LocalDateTime.now().plusDays(1))
    viewModel.loadCurrentWeather()
    forecastViewModel.loadForecastWeather()

    LaunchedEffect(key1 = true) {
        viewModel.eventFlow.collectLatest { event ->
            when (event) {
                is CurrentWeatherViewModel.UIEvent.ShowSnackBar -> {
                    scaffoldState.snackbarHostState.showSnackbar(
                        message = event.message
                    )
                }
            }
        }
    }
    LaunchedEffect(key1 = true) {
        forecastViewModel.eventFlow.collectLatest { event ->
            when (event) {
                is ForecastWeatherViewModel.UIEvent.ShowSnackBar -> {
                    scaffoldState.snackbarHostState.showSnackbar(
                        message = event.message
                    )
                }
            }
        }
    }
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
//        verticalArrangement = Arrangement.SpaceBetween
    ) {
        Row(
            modifier = myModifier,
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            when (state.currentWeatherItem?.weather?.get(0)?.main) {
                "Clouds" -> {
                    val painter = painterResource(id = R.drawable.sea_cloudy)
                    val currTemp = state.currentWeatherItem?.main?.temp
                    val weatherDes = "Cloudy"
                    val averageTemp = state.currentWeatherItem?.main.temp
                    val minTemp = state.currentWeatherItem?.main.temp_min
                    val maxTemp = state.currentWeatherItem?.main.temp_max
                    CurrentWeatherComponent(
                        painter = painter,
                        currentTemperature = currTemp,
                        weatherDescription = weatherDes,
                        minTemperature = minTemp,
                        averageTemperature = averageTemp,
                        maxTemperature = maxTemp
                    )
                }
                "Clear" -> {
                    val painter = painterResource(id = R.drawable.sea_sunnypng)
                    val currTemp = state.currentWeatherItem.main.temp
                    val weatherDes = "Sunny"
                    val averageTemp = state.currentWeatherItem.main.temp
                    val minTemp = state.currentWeatherItem.main.temp_min
                    val maxTemp = state.currentWeatherItem.main.temp_max
                    CurrentWeatherComponent(
                        painter = painter,
                        currentTemperature = currTemp,
                        weatherDescription = weatherDes,
                        minTemperature = minTemp,
                        averageTemperature = averageTemp,
                        maxTemperature = maxTemp
                    )


                }
                "Rain" -> {
                    val painter = painterResource(id = R.drawable.sea_rainy)
                    val currTemp = state.currentWeatherItem.main.temp
                    val weatherDes = "Rainy"
                    val averageTemp = state.currentWeatherItem.main.temp
                    val minTemp = state.currentWeatherItem.main.temp_min
                    val maxTemp = state.currentWeatherItem.main.temp_max
                    CurrentWeatherComponent(
                        painter = painter,
                        currentTemperature = currTemp,
                        weatherDescription = weatherDes,
                        minTemperature = minTemp,
                        averageTemperature = averageTemp,
                        maxTemperature = maxTemp
                    )
                }
            }

        }
        Row(
            modifier = myModifier
        ) {
            Spacer(modifier = Modifier.height(8.dp))
            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.SpaceBetween
            ) {


                println(arrayInterval)
                arrayInterval.forEach { interval ->
                    Row(
                        modifier = Modifier
                            .fillMaxWidth(),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.SpaceAround
                    ) {
                        Text(
                            text = LocalDateTime.parse(
                                interval.dt_txt,
                                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
                            ).dayOfWeek.toString(),
                            color = Color.White,
                            fontWeight = FontWeight.Bold
                        )
                        when(interval.weather[0].main){
                             "Clouds" ->{
                                 Image(modifier = Modifier.height(16.dp).width(16.dp),  painter = painterResource(id = R.drawable.partlysunny), contentDescription ="cloudy" )
                            }
                            "Clear" ->{
                                Image(modifier = Modifier.height(16.dp).width(16.dp),  painter = painterResource(id = R.drawable.clear), contentDescription ="cloudy" )
                            }
                            "Rain" ->{
                                Image(modifier = Modifier.height(16.dp).width(16.dp),  painter = painterResource(id = R.drawable.rain), contentDescription ="cloudy" )
                            }
                        }

                        Text(text = interval.main.temp.toString() + "\u2103", color = Color.White)
                    }
                }



            }
        }
    }


}


@Composable
fun CurrentWeatherComponent(
    painter: Painter,
    currentTemperature: Double,
    weatherDescription: String,
    minTemperature: Double,
    averageTemperature: Double,
    maxTemperature: Double,
    modifier: Modifier = Modifier
) {
    Column(
    ) {
        Box(

        ) {
            Image(
                painter = painter,
                contentDescription = "image",
                contentScale = ContentScale.Crop,
                modifier = Modifier.fillMaxWidth()

            )
            Column(
                modifier = Modifier
                    .fillMaxHeight(0.5f)
                    .fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                Text(
                    currentTemperature.toString() + "\u2103",
                    fontSize = 32.sp,
                    color = Color.White
                )
                Text(
                    weatherDescription,
                    fontSize = 48.sp,
                    color = Color.White,
                    fontWeight = FontWeight.Bold
                )
            }

        }

        Row() {
            Column {
                Row(
                    modifier = Modifier
                        .fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceAround
                ) {
                    Text(minTemperature.toString() + "\u2103", color = Color.White)
                    Text(text = averageTemperature.toString() + "\u2103", color = Color.White)
                    Text(text = maxTemperature.toString() + "\u2103", color = Color.White)
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceAround
                ) {
                    Text(text = "min", color = Color.White)
                    Text(text = "current", color = Color.White)
                    Text(text = "max", color = Color.White)
                }
                Spacer(
                    modifier = Modifier.height(4.dp)
                )
                Divider(
                    color = Color.White,
                    thickness = 2.dp
                )
            }
        }

    }
}
