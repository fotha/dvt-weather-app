package com.dvt.dvtweatherapp.presentation

import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.dvt.dvtweatherapp.presentation.favourates.FavouratesScreen
import com.dvt.dvtweatherapp.presentation.home.components.HomeScreen
import com.dvt.dvtweatherapp.presentation.places.PlacesScreen

@Composable
fun BottomNavGraph(navController: NavHostController){
    NavHost(
        navController = navController,
        startDestination = BottomBarScreen.Home.route
    ){
        composable(route =BottomBarScreen.Home.route){
            HomeScreen()
        }
        composable(route = BottomBarScreen.Places.route){
            PlacesScreen()
        }
        composable(route = BottomBarScreen.Favourates.route){
            FavouratesScreen()
        }
    }

}