package com.dvt.dvtweatherapp.presentation

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Place
import androidx.compose.ui.graphics.vector.ImageVector

sealed class BottomBarScreen(
    val route: String,
    val title: String,
    val icon: ImageVector
){
    object Home: BottomBarScreen(
        route = "home",
        title = "Home",
        icon = Icons.Default.Home
    )
    object Places: BottomBarScreen(
        route = "places",
        title = "Places",
        icon = Icons.Default.Place
    )
    object Favourates: BottomBarScreen(
        route = "favourates",
        title = "Favourates",
        icon = Icons.Default.Favorite
    )
}
