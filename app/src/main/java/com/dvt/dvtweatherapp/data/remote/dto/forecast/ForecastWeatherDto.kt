package com.dvt.dvtweatherapp.data.remote.dto.forecast

import com.dvt.dvtweatherapp.domain.model.CurrentWeather
import com.dvt.dvtweatherapp.domain.model.ForecastWeather

data class ForecastWeatherDto(
    val city: CityDto,
    val cnt: Int,
    val cod: String,
    val list: List<IntervalDto>,
    val message: Int
) {
    fun toForecastWeather(): ForecastWeather {
        return ForecastWeather(
            list= list.map { it.toInterval()},
        )
    }
}