package com.dvt.dvtweatherapp.data.remote.dto.forecast

import com.dvt.dvtweatherapp.data.remote.dto.*
import com.dvt.dvtweatherapp.domain.model.Interval
import com.dvt.dvtweatherapp.domain.model.Weather

data class IntervalDto(
    val clouds: CloudsDto,
    val dt: Int,
    val dt_txt: String,
    val main: MainDto,
    val pop: Double,
    val rain: RainDto,
    val sys: SysForecastDto,
    val visibility: Int,
    val weather: List<WeatherDto>,
    val wind: WindDto
){
    fun toInterval(): Interval {
        return  Interval(
            dt_txt =dt_txt,
            main = main,
            weather = weather.map { it.toWeather() }
        )
    }
}
