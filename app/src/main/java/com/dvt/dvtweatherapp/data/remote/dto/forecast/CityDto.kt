package com.dvt.dvtweatherapp.data.remote.dto.forecast

import com.dvt.dvtweatherapp.data.remote.dto.CoordDto

data class CityDto(
    val coord: CoordDto,
    val country: String,
    val id: Int,
    val name: String,
    val population: Int,
    val sunrise: Int,
    val sunset: Int,
    val timezone: Int
)