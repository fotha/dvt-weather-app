package com.dvt.dvtweatherapp.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.dvt.dvtweatherapp.domain.model.CurrentWeather
import com.dvt.dvtweatherapp.domain.model.Main
import com.dvt.dvtweatherapp.domain.model.Weather

@Entity
data class CurrentWeatherEntity(
    @PrimaryKey val id: Int? = null,
    val main: Main,
    val name: String,
    val weather: List<Weather>
){
    fun toCurrentWeather(): CurrentWeather{
        return  CurrentWeather(
            main = main,
            name = name,
            weather = weather
        )
    }
}
