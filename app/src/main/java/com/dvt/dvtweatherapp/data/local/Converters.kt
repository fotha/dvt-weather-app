package com.dvt.dvtweatherapp.data.local

import androidx.room.ProvidedTypeConverter
import androidx.room.TypeConverter
import com.dvt.dvtweatherapp.data.local.util.JsonParser
import com.dvt.dvtweatherapp.domain.model.Main
import com.dvt.dvtweatherapp.domain.model.Weather
import com.google.gson.reflect.TypeToken

@ProvidedTypeConverter
class Converters(
    private val jsonParser: JsonParser
) {
    @TypeConverter
    fun fromWeatherJson(json: String): List<Weather>{
        return jsonParser.fromJson<ArrayList<Weather>>(
            json,
            object: TypeToken<ArrayList<Weather>>(){}.type
        )?: emptyList()
    }
    @TypeConverter
    fun toWeatherJson(weather: List<Weather>): String{
        return jsonParser.toJson(
            weather,
            object: TypeToken<ArrayList<Weather>>(){}.type
        )?: "[]"
    }

    @TypeConverter
    fun fromMainJson(json: String): Main {
        return jsonParser.fromJson<Main>(
            json,
            object: TypeToken<Main>(){}.type
        )?: Main(10.1,10.1, 10.1)
    }
    @TypeConverter
    fun toMainJson(main: Main): String{
        return jsonParser.toJson(
            main,
            object: TypeToken<Main>(){}.type
        )?: "[]"
    }
}