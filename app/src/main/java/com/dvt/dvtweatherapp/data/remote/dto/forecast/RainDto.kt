package com.dvt.dvtweatherapp.data.remote.dto.forecast

data class RainDto(
    val `3h`: Double
)