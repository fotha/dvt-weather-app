package com.dvt.dvtweatherapp.data.repository

import android.Manifest
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import androidx.core.app.ActivityCompat
import com.dvt.dvtweatherapp.R
import com.dvt.dvtweatherapp.WeatherApp
import com.dvt.dvtweatherapp.common.Constants
import com.dvt.dvtweatherapp.common.Resource
import com.dvt.dvtweatherapp.data.local.CurrentWeatherDao
import com.dvt.dvtweatherapp.data.remote.OpenWeatherAPI
import com.dvt.dvtweatherapp.domain.model.CurrentWeather
import com.dvt.dvtweatherapp.domain.repository.CurrentWeatherRepository
import com.dvt.dvtweatherapp.domain.use_case.GetCurrentLocation
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okio.IOException
import retrofit2.HttpException
import java.util.*

// A concrete Implementation of CurrentWeather interface
class CurrentWeatherRepositoryImp(
    private val api: OpenWeatherAPI,
    private val dao: CurrentWeatherDao,
    private val location: GetCurrentLocation
): CurrentWeatherRepository {
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    // has a new Location
    private lateinit var locationCallback: LocationCallback

    // This will store current location info
    private var currentLocation: Location? = null
    var latitude: Double? = null
    var longitude: Double? = null
    override fun getCurrentWeather(): Flow<Resource<CurrentWeather>> = flow {
        emit(Resource.Loading())

        val currentWeather = dao.getCurrentWeather()
        emit(Resource.Loading(data = currentWeather))


        try {
            println("from use case")
//            println(location.invoke().lat)
            findLocation()
            val remoteCurrentWeather =
                api.getCurrentWeather(latitude.toString(), longitude.toString(), "metric", Constants.API_KEY)
            dao.deleteCurrentWeather()
            dao.insertCurrentWeather(remoteCurrentWeather.currentWeatherEntity())

        } catch (e: HttpException) {
            emit(
                Resource.Error(
                    message = R.string.http_error.toString(),
                    data = currentWeather
                )
            )

        } catch (e: IOException) {
            emit(
                Resource.Error(
                    message = R.string.io_error.toString(),
                    data = currentWeather
                )
            )
        }

        val newCurrentWeather = dao.getCurrentWeather()
        emit(Resource.Success(newCurrentWeather))
    }

    fun findLocation() {
        println("Ke ho kae?")
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(WeatherApp.instance)
        if (ActivityCompat.checkSelfPermission(
                WeatherApp.instance,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                WeatherApp.instance,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        fusedLocationClient.lastLocation.addOnCompleteListener() {
            val location: Location? = it.result
            if (location != null) {
                val geocoder = Geocoder(WeatherApp.instance, Locale.getDefault())
                val list: List<Address> =
                    geocoder.getFromLocation(location.latitude, location.longitude, 1)
                latitude = location.latitude
                longitude = location.longitude
                println(list[0].countryName)

            }


        }

    }
}