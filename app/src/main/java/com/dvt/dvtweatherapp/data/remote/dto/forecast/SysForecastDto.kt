package com.dvt.dvtweatherapp.data.remote.dto.forecast

data class SysForecastDto(
    val pod: String
)