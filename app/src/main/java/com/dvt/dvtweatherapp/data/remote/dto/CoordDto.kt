package com.dvt.dvtweatherapp.data.remote.dto

data class CoordDto(
    val lat: Double,
    val lon: Double
)