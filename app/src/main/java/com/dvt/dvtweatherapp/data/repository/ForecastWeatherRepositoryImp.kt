package com.dvt.dvtweatherapp.data.repository

import android.Manifest
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import androidx.core.app.ActivityCompat
import com.dvt.dvtweatherapp.R
import com.dvt.dvtweatherapp.WeatherApp
import com.dvt.dvtweatherapp.common.Constants
import com.dvt.dvtweatherapp.common.Resource
import com.dvt.dvtweatherapp.data.remote.OpenWeatherAPI
import com.dvt.dvtweatherapp.domain.model.ForecastWeather
import com.dvt.dvtweatherapp.domain.repository.ForecastWeatherRepository
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationServices
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import java.util.*

class ForecastWeatherRepositoryImp(
    private val api: OpenWeatherAPI
): ForecastWeatherRepository {
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    var latitude: Double? = null
    var longitude: Double? = null
    override fun getForecastWeather(): Flow<Resource<ForecastWeather>> = flow{
//        emit(Resource.Loading())
        findLocation()

        try {
            val forecastWeather = api.getForecastWeather(latitude.toString(),longitude.toString(),"metric", Constants.API_KEY)
            emit(Resource.Success(forecastWeather.toForecastWeather()))
        }catch (e: HttpException){
//            emit(Resource.Error(
//                message = "Ops something went wrong",
//                data = currentWeather
//            ))
        }catch (e: IOException){
//            emit(Resource.Error(
//                message = "Trouble with connection, Check your internet",
//                data = currentWeather
//            ))
        }
    }
    fun findLocation() {
        println("Ke ho kae?")
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(WeatherApp.instance)
        if (ActivityCompat.checkSelfPermission(
                WeatherApp.instance,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                WeatherApp.instance,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        fusedLocationClient.lastLocation.addOnCompleteListener() {
            val location: Location? = it.result
            if (location != null) {
                val geocoder = Geocoder(WeatherApp.instance, Locale.getDefault())
                val list: List<Address> =
                    geocoder.getFromLocation(location.latitude, location.longitude, 1)
                latitude = location.latitude
                longitude = location.longitude
                println(list[0].countryName)

            }


        }

    }
}