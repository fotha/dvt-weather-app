package com.dvt.dvtweatherapp.data.remote.dto

data class WindDto(
    val deg: Int,
    val speed: Double
)