package com.dvt.dvtweatherapp.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.dvt.dvtweatherapp.data.local.entity.CurrentWeatherEntity

@Database(
    entities = [CurrentWeatherEntity::class],
    version = 1
)
@TypeConverters(Converters::class)
abstract class WeatherDatabase: RoomDatabase() {
    abstract val currentWeatherDao: CurrentWeatherDao
}