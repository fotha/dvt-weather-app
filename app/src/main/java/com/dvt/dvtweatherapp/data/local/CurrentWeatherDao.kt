package com.dvt.dvtweatherapp.data.local

import androidx.room.*
import com.dvt.dvtweatherapp.data.local.entity.CurrentWeatherEntity
import com.dvt.dvtweatherapp.domain.model.CurrentWeather

@Dao
interface CurrentWeatherDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCurrentWeather(currentWeather: CurrentWeatherEntity)

    @Query("DELETE FROM currentweatherentity")
    suspend fun deleteCurrentWeather()

    @Query("SELECT * FROM currentweatherentity")
    suspend fun getCurrentWeather(): CurrentWeather
}