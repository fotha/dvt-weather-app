package com.dvt.dvtweatherapp.data.remote.dto

import com.dvt.dvtweatherapp.domain.model.Weather

data class WeatherDto(
    val description: String,
    val icon: String,
    val id: Int,
    val main: String
){
    fun toWeather(): Weather{
        return  Weather(
            main = main
        )
    }
}