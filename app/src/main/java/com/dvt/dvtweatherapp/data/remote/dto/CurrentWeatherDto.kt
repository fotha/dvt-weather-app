package com.dvt.dvtweatherapp.data.remote.dto

import com.dvt.dvtweatherapp.data.local.entity.CurrentWeatherEntity
import com.dvt.dvtweatherapp.domain.model.CurrentWeather

data class CurrentWeatherDto(
    val base: String,
    val clouds: CloudsDto,
    val cod: Int,
    val coord: CoordDto,
    val dt: Int,
    val id: Int,
    val main: MainDto,
    val name: String,
    val sys: SysDto,
    val visibility: Int,
    val weather: List<WeatherDto>,
    val wind: WindDto
){
    fun currentWeather(): CurrentWeather{
        return  CurrentWeather(
            main = main.toMain(),
            name = name,
            weather = weather.map { it.toWeather() }
        )
    }
    fun currentWeatherEntity(): CurrentWeatherEntity{
        return  CurrentWeatherEntity(
            main = main.toMain(),
            name = name,
            weather = weather.map { it.toWeather() }
        )
    }
}