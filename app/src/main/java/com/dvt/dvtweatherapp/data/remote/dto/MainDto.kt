package com.dvt.dvtweatherapp.data.remote.dto

import com.dvt.dvtweatherapp.domain.model.Main

data class MainDto(
    val humidity: Int,
    val pressure: Int,
    val temp: Double,
    val temp_max: Double,
    val temp_min: Double
) {
    fun toMain(): Main{
        return Main(
            temp = temp,
            temp_max = temp_max,
            temp_min = temp_min
        )
    }
}