package com.dvt.dvtweatherapp.data.remote

import com.dvt.dvtweatherapp.data.remote.dto.CurrentWeatherDto
import com.dvt.dvtweatherapp.data.remote.dto.forecast.ForecastWeatherDto
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenWeatherAPI {

    //get current weather
    @GET("weather?")
    suspend fun getCurrentWeather(@Query("lat") lat: String, @Query("lon") lon: String, @Query("units") units: String, @Query("APPID") app_id: String): CurrentWeatherDto

    //get d day forecast
    @GET("forecast?")
    suspend fun getForecastWeather(@Query("lat") lat: String, @Query("lon") lon: String, @Query("units") units: String, @Query("APPID") app_id: String): ForecastWeatherDto
}