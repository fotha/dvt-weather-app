package com.dvt.dvtweatherapp.di

import android.app.Application
import androidx.room.Room
import com.dvt.dvtweatherapp.common.Constants
import com.dvt.dvtweatherapp.data.local.Converters
import com.dvt.dvtweatherapp.data.local.CurrentWeatherDao
import com.dvt.dvtweatherapp.data.local.WeatherDatabase
import com.dvt.dvtweatherapp.data.local.util.GsonParser
import com.dvt.dvtweatherapp.data.remote.OpenWeatherAPI
import com.dvt.dvtweatherapp.data.repository.CurrentWeatherRepositoryImp
import com.dvt.dvtweatherapp.data.repository.ForecastWeatherRepositoryImp
import com.dvt.dvtweatherapp.domain.repository.CurrentWeatherRepository
import com.dvt.dvtweatherapp.domain.repository.ForecastWeatherRepository
import com.dvt.dvtweatherapp.domain.use_case.GetCurrentLocation
import com.dvt.dvtweatherapp.domain.use_case.GetCurrentWeather
import com.dvt.dvtweatherapp.domain.use_case.GetForecastWeather
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object CurrentWeatherModule {

    @Provides
    @Singleton
    fun provideGetCurrentWeatherUseCase(repository: CurrentWeatherRepository): GetCurrentWeather{
        return GetCurrentWeather(repository)
    }

    @Provides
    @Singleton
    fun provideGetForecastWeatherUseCase(repository: ForecastWeatherRepository): GetForecastWeather{
        return  GetForecastWeather(repository)
    }
    @Provides
    @Singleton
    fun provideGetCurrentLocationUseCase(): GetCurrentLocation {
        return  GetCurrentLocation()
    }

    @Provides
    @Singleton
    fun provideCurrentWeatherRepository(
        db: WeatherDatabase,
        api: OpenWeatherAPI,
        location: GetCurrentLocation
    ): CurrentWeatherRepository{
        return CurrentWeatherRepositoryImp(api, db.currentWeatherDao, location)
    }

    @Provides
    @Singleton
    fun provideForecastWeatherRepository(
        api: OpenWeatherAPI
    ): ForecastWeatherRepository{
        return  ForecastWeatherRepositoryImp(api)
    }

    @Provides
    @Singleton
    fun provideWeatherDatabase(app: Application): WeatherDatabase{
        return Room.databaseBuilder(
            app,
            WeatherDatabase::class.java,
            "weather_db"
        ).addTypeConverter(Converters(GsonParser(Gson())))
            .build()
    }

    @Provides
    @Singleton
    fun provideOpenWeatherApi(): OpenWeatherAPI {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(OpenWeatherAPI::class.java)
    }
}